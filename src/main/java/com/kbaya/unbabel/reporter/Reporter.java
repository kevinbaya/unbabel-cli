package com.kbaya.unbabel.reporter;

import java.time.Instant;

public interface Reporter {
    void start();
    void stop();
    void setCurrentInstant(Instant instant);
}
