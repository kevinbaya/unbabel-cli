package com.kbaya.unbabel.reporter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.Instant;
import java.util.Map;

@JsonSerialize(using = ReportLineSerializer.class)
class ReportLine {
    private final Instant timestamp;
    private final Map<String,Double> metricMap;

    ReportLine(Instant timestamp, Map<String,Double> metricMap) {
        this.timestamp = timestamp;
        this.metricMap = metricMap;
    }

    Instant getTimestamp() {
        return timestamp;
    }

    Map<String,Double> getMetricMap() {
        return metricMap;
    }
}
