package com.kbaya.unbabel.reporter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class ReportLineSerializer extends JsonSerializer<ReportLine> {

    @Override
    public void serialize(ReportLine reportLine, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);

        jsonGenerator.writeStartObject();
        String timestamp = formatter.format(reportLine.getTimestamp());
        jsonGenerator.writeStringField("timestamp", timestamp);
        for(Map.Entry<String, Double> entry: reportLine.getMetricMap().entrySet()){
            jsonGenerator.writeNumberField(entry.getKey(), entry.getValue());
        }
        jsonGenerator.writeEndObject();
    }
}
