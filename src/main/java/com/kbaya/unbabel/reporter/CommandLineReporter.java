package com.kbaya.unbabel.reporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kbaya.unbabel.metric.MetricGroup;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

public class CommandLineReporter implements Reporter {

    private final MetricGroup metricGroup;
    private Instant currentInstant;
    private Instant lastInstant;
    private boolean started;

    public CommandLineReporter(MetricGroup metricGroup) {
        this.metricGroup = metricGroup;
        this.started = false;
    }

    @Override
    public void start() {
        currentInstant = null;
        lastInstant = null;
        this.started = true;
    }

    @Override
    public void stop() {
        this.setCurrentInstant(currentInstant.truncatedTo(ChronoUnit.MINUTES).plus(Duration.ofMinutes(1)));
        this.started = false;
    }

    @Override
    public void setCurrentInstant(Instant instant) {
        if(currentInstant == null) {
            currentInstant = instant;
        } else {
            lastInstant = currentInstant;
            currentInstant = instant;
            if(started) {
                reportAllMissingLines();
            }
        }

    }

    private void reportAllMissingLines() {
        Instant currentInstantTruncated = currentInstant.truncatedTo(ChronoUnit.MINUTES);
        Instant lastInstantTruncated = lastInstant.truncatedTo(ChronoUnit.MINUTES);

        long nbOfMinutes = Duration.between(lastInstantTruncated, currentInstantTruncated).toMinutes();
        Instant reportInstant = lastInstantTruncated;
        for (int i = 0; i < nbOfMinutes;  i++) {
            reportOneLine(reportInstant);
            reportInstant = reportInstant.plus(Duration.ofMinutes(1));
        }

    }

    void reportOneLine(Instant instant) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Double> metricMap = metricGroup.getMetricMap();
        ReportLine reportLine = new ReportLine(instant, metricMap);
        try {
            String jsonReport = mapper.writeValueAsString(reportLine);
            System.out.println(jsonReport);
        } catch(JsonProcessingException e) {
            throw new RuntimeException("Failure to generate proper json for the report line.");
        }
    }

}
