package com.kbaya.unbabel.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Event {
    private String timestamp;
    private String translationId;
    private String sourceLanguage;
    private String targetLanguage;
    private String clientName;
    private String eventName;
    private Integer duration;
    private Integer nrWords;

    @JsonCreator
    public Event(@JsonProperty("timestamp") String timestamp,
                 @JsonProperty("translation_id") String translationId,
                 @JsonProperty("source_language") String sourceLanguage,
                 @JsonProperty("target_language") String targetLanguage,
                 @JsonProperty("client_name") String clientName,
                 @JsonProperty("event_name") String eventName,
                 @JsonProperty("duration") Integer duration,
                 @JsonProperty("nr_words") Integer nrWords) {
        this.timestamp = timestamp;
        this.translationId = translationId;
        this.sourceLanguage = sourceLanguage;
        this.targetLanguage = targetLanguage;
        this.clientName = clientName;
        this.eventName = eventName;
        this.duration = duration;
        this.nrWords = nrWords;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTranslationId() {
        return translationId;
    }

    public String getSourceLanguage() {
        return sourceLanguage;
    }

    public String getTargetLanguage() {
        return targetLanguage;
    }

    public String getClientName() {
        return clientName;
    }

    public String getEventName() {
        return eventName;
    }

    public Integer getDuration() {
        return duration;
    }

    public Integer getNrWords() {
        return nrWords;
    }
}
