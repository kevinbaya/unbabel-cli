package com.kbaya.unbabel.event;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class FileEventReader implements EventReader {

    private final BufferedReader reader;
    private final ObjectMapper mapper;


    public FileEventReader(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        FileReader fileReader = new FileReader(file);
        reader = new BufferedReader(fileReader);
        mapper = new ObjectMapper();
    }

    public Event nextEvent() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            return null;
        } else {
            return mapper.readValue(line, Event.class);
        }
    }
}
