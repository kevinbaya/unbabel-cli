package com.kbaya.unbabel.event;

import java.io.IOException;

public interface EventReader {
    Event nextEvent() throws IOException;
}
