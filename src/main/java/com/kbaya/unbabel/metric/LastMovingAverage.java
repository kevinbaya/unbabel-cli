package com.kbaya.unbabel.metric;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.Queue;

public class LastMovingAverage implements Metric {
    private final int windowSize;
    private Queue<DataPoint> lastDataPointList;
    private Double currentSum;

    public LastMovingAverage(int windowSize) {
        this.windowSize = windowSize;
        this.lastDataPointList = new LinkedList<>();
        this.currentSum = 0.0;
    }

    @Override
    public void processNewValue(Instant instant, Double value) {
        DataPoint newDataPoint = new DataPoint(instant, value);
        lastDataPointList.offer(newDataPoint);
        currentSum += newDataPoint.getValue();

        DataPoint oldestDataPoint;
        while((oldestDataPoint = lastDataPointList.peek()) != null
                && Duration.between(oldestDataPoint.getInstant(), instant)
                .compareTo(Duration.ofSeconds(windowSize)) > 0 ) {
            DataPoint pointToDelete = lastDataPointList.poll();
            currentSum = currentSum - pointToDelete.getValue();
        }
    }

    @Override
    public Double getAggregatedValue() {
        if(lastDataPointList.size() == 0){
            throw new IllegalStateException("You can't invoke getAggregatedValue when there is no data points !");
        }
        return currentSum / (double) lastDataPointList.size();
    }
}
