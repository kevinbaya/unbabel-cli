package com.kbaya.unbabel.metric;

import java.time.Instant;

public class DataPoint {
    private final Instant instant;
    private final Double value;

    DataPoint(Instant instant, Double value) {
        this.instant = instant;
        this.value = value;
    }

    public Instant getInstant() {
        return instant;
    }

    public Double getValue() {
        return value;
    }


}
