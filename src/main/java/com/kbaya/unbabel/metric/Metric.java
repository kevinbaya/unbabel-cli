package com.kbaya.unbabel.metric;

import java.time.Instant;

public interface Metric {
    void processNewValue(Instant instant, Double value);
    Double getAggregatedValue();
}
