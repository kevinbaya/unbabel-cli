package com.kbaya.unbabel.metric;

import java.util.HashMap;
import java.util.Map;

public class MetricGroup {
    private final Map<String, Metric> mapNameToMetric;

    public MetricGroup() {
        mapNameToMetric = new HashMap<>();
    }

    public void add(String name, Metric metric){
        mapNameToMetric.put(name, metric);
    }

    public Map<String, Double> getMetricMap() {
        Map<String, Double> result = new HashMap<>();
        for (Map.Entry<String, Metric> entry : mapNameToMetric.entrySet()) {
            result.put(entry.getKey(), entry.getValue().getAggregatedValue());
        }
        return result;
    }
}
