package com.kbaya.unbabel;

import com.kbaya.unbabel.event.Event;
import com.kbaya.unbabel.event.EventReader;
import com.kbaya.unbabel.event.FileEventReader;
import com.kbaya.unbabel.metric.LastMovingAverage;
import com.kbaya.unbabel.metric.Metric;
import com.kbaya.unbabel.metric.MetricGroup;
import com.kbaya.unbabel.reporter.CommandLineReporter;
import com.kbaya.unbabel.reporter.Reporter;
import org.apache.commons.cli.*;

import java.io.*;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        Options options = new Options();
        options.addOption(Option.builder(null)
                .required()
                .hasArg(true)
                .argName("file_name")
                .desc("Input file with the list of events")
                .longOpt("input")
                .build());
        options.addOption(Option.builder(null)
                .required()
                .hasArg(true)
                .argName("window")
                .desc("Size of the time window")
                .longOpt("window-size")
                .type(Number.class)
                .build());

        CommandLineParser parser = new DefaultParser();

        int windowSize;
        String fileName;
        try {
            CommandLine commandLine = parser.parse(options, args);
            fileName = commandLine.getOptionValue("input");
            windowSize = ((Number) commandLine.getParsedOptionValue("window-size")).intValue();

        } catch (ParseException e) {
            System.err.println("Command args parsing failed : " + e.getMessage());
            return;
        }
        EventReader eventReader;
        try {
            eventReader = new FileEventReader(fileName);
        } catch(FileNotFoundException e) {
            System.err.println("File not found : " + e.getMessage());
            return;
        }

        Metric averageDeliveryTime = new LastMovingAverage(windowSize * 60);
        MetricGroup metricGroup = new MetricGroup();
        metricGroup.add("average_delivery_time", averageDeliveryTime);
        Reporter reporter = new CommandLineReporter(metricGroup);
        reporter.start();

        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                .withZone(ZoneOffset.UTC);
        try {
            Event event;
            while ((event = eventReader.nextEvent()) != null) {
                Instant instant = Instant.from(formatter.parse(event.getTimestamp()));
                reporter.setCurrentInstant(instant);
                averageDeliveryTime.processNewValue(instant, event.getDuration().doubleValue());
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        reporter.stop();

    }
}
