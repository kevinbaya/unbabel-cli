package com.kbaya.unbabel.reporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ReportLineSerializerTest {

    @Test
    public void serializerReturnsExpectedJsonString() throws JsonProcessingException {
        Map<String,Double> metricMap = new HashMap<>();
        metricMap.put("metric_a", 10.0);
        metricMap.put("metric_b", 15.0);
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);
        Instant instant = Instant.from(formatter.parse("2019-02-20 16:30:17"));
        ReportLine reportLine = new ReportLine(instant, metricMap);
        ObjectMapper mapper = new ObjectMapper();
        String expectedJson = "{\"timestamp\":\"2019-02-20 16:30:17\",\"metric_b\":15.0,\"metric_a\":10.0}";
        String actualJson =  mapper.writeValueAsString(reportLine);
        Assert.assertEquals(expectedJson, actualJson);
    }
}
