package com.kbaya.unbabel.reporter;

import com.kbaya.unbabel.metric.Metric;
import com.kbaya.unbabel.metric.MetricGroup;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class CommandLineReporterTest {
    private final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errStream = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outStream));
        System.setErr(new PrintStream(errStream));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void reportedOneLineProducesCorrectJson() {
        CommandLineReporter commandLineReporter = this.generateReporter();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);
        Instant instant = Instant.from(formatter.parse("2019-02-20 16:30:17"));

        commandLineReporter.reportOneLine(instant);
        String expectedJson = "{\"timestamp\":\"2019-02-20 16:30:17\",\"metric_b\":30.0,\"metric_a\":10.0}";
        assertEquals(expectedJson, outStream.toString().trim());

    }

    @Test
    public void reportNothingWhenNoMinuteHasPassed() {
        CommandLineReporter commandLineReporter = this.generateReporter();
        commandLineReporter.start();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);
        Instant instant1 = Instant.from(formatter.parse("2019-02-20 16:30:17"));
        Instant instant2 = Instant.from(formatter.parse("2019-02-20 16:30:59"));

        commandLineReporter.setCurrentInstant(instant1);
        commandLineReporter.setCurrentInstant(instant2);
        assertEquals("", outStream.toString().trim());

    }


    @Test
    public void reportOneLineIfOneMinuteHasPassed() {
        CommandLineReporter commandLineReporter = this.generateReporter();
        commandLineReporter.start();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);
        Instant instant1 = Instant.from(formatter.parse("2019-02-20 16:30:17"));
        Instant instant2 = Instant.from(formatter.parse("2019-02-20 16:31:01"));

        commandLineReporter.setCurrentInstant(instant1);
        commandLineReporter.setCurrentInstant(instant2);
        String expectedJson = "{\"timestamp\":\"2019-02-20 16:30:00\",\"metric_b\":30.0,\"metric_a\":10.0}";
        assertEquals(expectedJson, outStream.toString().trim());

    }


    @Test
    public void reportThreeLineIfThreeMinutesHasPassed() {
        CommandLineReporter commandLineReporter = this.generateReporter();
        commandLineReporter.start();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneOffset.UTC);
        Instant instant1 = Instant.from(formatter.parse("2019-02-20 16:30:17"));
        Instant instant2 = Instant.from(formatter.parse("2019-02-20 16:33:01"));

        commandLineReporter.setCurrentInstant(instant1);
        commandLineReporter.setCurrentInstant(instant2);
        String expectedOutput = "{\"timestamp\":\"2019-02-20 16:30:00\",\"metric_b\":30.0,\"metric_a\":10.0}" +
                "\n" +
                "{\"timestamp\":\"2019-02-20 16:31:00\",\"metric_b\":30.0,\"metric_a\":10.0}" +
                "\n" +
                "{\"timestamp\":\"2019-02-20 16:32:00\",\"metric_b\":30.0,\"metric_a\":10.0}";
        assertEquals(expectedOutput, outStream.toString().trim());

    }



    private CommandLineReporter generateReporter() {
        Metric mockedMetricA = Mockito.mock(Metric.class);
        Double mockedValueA = 10.0;

        Metric mockedMetricB = Mockito.mock(Metric.class);
        Double mockedValueB = 30.0;

        Mockito.doReturn(mockedValueA).when(mockedMetricA).getAggregatedValue();
        Mockito.doReturn(mockedValueB).when(mockedMetricB).getAggregatedValue();

        MetricGroup metricGroup = new MetricGroup();
        metricGroup.add("metric_a", mockedMetricA);
        metricGroup.add("metric_b", mockedMetricB);

        return new CommandLineReporter(metricGroup);
    }
}
