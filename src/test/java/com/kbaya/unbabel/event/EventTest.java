package com.kbaya.unbabel.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EventTest {
    private static ObjectMapper mapper;
    private static String eventJson;

    @BeforeClass
    public static void setup() throws IOException {
        mapper = new ObjectMapper();
        eventJson = new String(Files.readAllBytes(Paths.get("src/test/java/resources/event_example.json")));
    }

    @Test
    public void hasCorrectTimestamp() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("2018-12-26 18:11:08.509654", event.getTimestamp());
    }

    @Test
    public void hasCorrectTranslationId() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("5aa5b2f39f7254a75aa5", event.getTranslationId());
    }

    @Test
    public void hasCorrectSourceLanguage() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("en", event.getSourceLanguage());
    }

    @Test
    public void hasCorrectTargetLanguage() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("fr", event.getTargetLanguage());
    }

    @Test
    public void containsCorrectClientName() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("easyjet", event.getClientName());
    }

    @Test
    public void containsCorrectEventName() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals("translation_requested", event.getEventName());
    }

    @Test
    public void containsCorrectDuration() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals(Integer.valueOf(20), event.getDuration());
    }

    @Test
    public void containsCorrectNrWords() throws IOException {
        Event event = mapper.readValue(eventJson, Event.class);
        Assert.assertEquals(Integer.valueOf(30), event.getNrWords());
    }
}
