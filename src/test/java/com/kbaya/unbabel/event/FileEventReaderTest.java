package com.kbaya.unbabel.event;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class FileEventReaderTest {
    private EventReader eventReader;

    @Before
    public void setup() throws FileNotFoundException {
        eventReader = new FileEventReader("src/test/java/resources/input_example.txt");
    }

    @Test
    public void nextEventReturnNonNullEventUntilTheEnd() throws IOException {
        Event eventA = eventReader.nextEvent();
        Event eventB = eventReader.nextEvent();
        Event eventC = eventReader.nextEvent();

        Assert.assertNotNull(eventA);
        Assert.assertNotNull(eventB);
        Assert.assertNotNull(eventC);

        Event eventD = eventReader.nextEvent();
        Assert.assertNull(eventD);
    }
}
