package com.kbaya.unbabel.metric;

import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;

public class LastMovingAverageTest {

    @Test
    public void movingAverageCalculationIsCorrect() {
        Metric movingAvg = new LastMovingAverage(10*60);

        Instant instantA = Instant.parse("2018-12-26T18:11:08.509654Z");
        Instant instantB = Instant.parse("2018-12-26T18:15:19.903159Z");
        Instant instantC = Instant.parse("2018-12-26T18:23:19.903159Z");

        Double valueA = 20.0;
        Double valueB = 31.0;
        Double valueC = 54.0;

        movingAvg.processNewValue(instantA, valueA);
        Assert.assertEquals(movingAvg.getAggregatedValue(), Double.valueOf(20));

        movingAvg.processNewValue(instantB, valueB);
        Assert.assertEquals(movingAvg.getAggregatedValue(), Double.valueOf(25.5));

        movingAvg.processNewValue(instantC, valueC);
        Assert.assertEquals(movingAvg.getAggregatedValue(), Double.valueOf(42.5));
    }

    @Test(expected = IllegalStateException.class)
    public void getAggregateValueWhenNoDataPoint() {
        Metric movingAvg = new LastMovingAverage(10*60);
        movingAvg.getAggregatedValue();
    }
}
