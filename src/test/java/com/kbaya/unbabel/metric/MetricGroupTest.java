package com.kbaya.unbabel.metric;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Map;

public class MetricGroupTest {


    @Test
    public void addingMetricsAndRetrievingThemAllWorks() {
        Metric mockedMetricA = Mockito.mock(Metric.class);
        Double mockedValueA = 10.0;

        Metric mockedMetricB = Mockito.mock(Metric.class);
        Double mockedValueB = 30.0;

        Mockito.doReturn(mockedValueA).when(mockedMetricA).getAggregatedValue();
        Mockito.doReturn(mockedValueB).when(mockedMetricB).getAggregatedValue();

        MetricGroup metricGroup = new MetricGroup();
        metricGroup.add("metric_a", mockedMetricA);
        metricGroup.add("metric_b", mockedMetricB);

        Map<String,Double> metricMap = metricGroup.getMetricMap();

        Assert.assertEquals(metricMap.entrySet().size(),2);
        Assert.assertEquals(metricMap.get("metric_a"), mockedValueA);
        Assert.assertEquals(metricMap.get("metric_b"), mockedValueB);

    }
}
