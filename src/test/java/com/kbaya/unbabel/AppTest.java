package com.kbaya.unbabel;

import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errStream = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outStream));
        System.setErr(new PrintStream(errStream));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void commandFailWithoutInput() {
        App.main(new String[] {"--window-size", "10"});
        assertEquals("Command args parsing failed : Missing required option: input", errStream.toString().trim());
    }

    @Test
    public void commandFailWithoutWindowSize() {
        App.main(new String[] {"--input", "myfile.txt"});
        assertEquals("Command args parsing failed : Missing required option: window-size", errStream.toString().trim());
    }

    @Test
    public void commandFailedWithNonExistingFilePath() {
        App.main(new String[] {"--input", "myfile.txt", "--window-size", "10"});
        assertEquals("File not found : myfile.txt (No such file or directory)", errStream.toString().trim());
    }

    @Test
    public void commandPrintExpectedOutputWithCorrectArgs() throws IOException {
        App.main(new String[] {"--input", "src/test/java/resources/input_example.txt", "--window-size", "10"});

        String expectedOutput= new String(Files.readAllBytes(Paths.get("src/test/java/resources/output_example.txt")));
        assertEquals("", errStream.toString().trim());
        assertEquals(expectedOutput, outStream.toString().trim());
    }

}
