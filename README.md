Requirements
---
- Java 8
- Maven 3

Test suite
---
~~~
mvn test
~~~

Running the program
---
~~~
mvn clean install
java -jar target/unbabel-cli-1.0-SNAPSHOT-jar-with-dependencies.jar --input src/test/java/resources/input_example.txt --window-size 10
~~~


Design choices
---
- EventReader interface 
    - Decouples the way inputs events are received from the rest of the script
    - Only current implementation : FileEventReader which reads events from the input file
- Metric interface
    - Help to easily implement other metrics in a decoupled way from the events and the reporting part
    - Only current implementation LastMovingAverage
- MetricGroup class
    - Hold a group of metrics together
- Reporter interface 
    - Interface to implement in order to manage how the metric group object is reported (dealing with the output io and report timing)
    - Only implementation : CommandLineReporter